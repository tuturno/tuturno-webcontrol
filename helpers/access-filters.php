<?php

/**
 * Middleware for routing and validating access
 **/
function validateCompanyAccess( $router ) {
	global $app;

	$params = $router->getParams();
	$companyCode = $params['company'];

	$companyObj = null;

	$service_url = TUTURNO_SERVER.'company/getByCodeWithQueues.json?';
    $params = array('code' => $companyCode);

    $response = serviceQuery( $service_url.http_build_query($params) );


	if( $response !== null && isset($response->company) ) { //service is active!

		// this variables will be used when we need to get data from the company and queues
		$companyObj = $response->company;
		$companyObj->queues = $response->queues;

		$companyObj->code = $companyCode;

		$app->company = $companyObj;

		foreach ($companyObj->queues as $queue) {
			if($queue->class=='app.tuturno.NumericQueue') {
				$app->company->current_queue = $queue;
				break;
			}
		}

		//die("<pre>".print_r($app->company, true)."</pre>");

		// here continue the user validation in the next Middleware (filter): validateUserLogin

	} elseif ($response !== null && $response->status) { //error: company not found

		$app->flashNow( 'server_errors', $response->message );

		$companyObj = new stdClass();
		$companyObj->id = 0;
		$companyObj->code = $companyCode;

	} else { // server off :(
		$app->flashNow( 'server_errors', 'SERVER NOT FOUND: '.TUTURNO_SERVER );

		$companyObj = new stdClass();
		$companyObj->id = 0;
		$companyObj->code = $companyCode;
	}

	$app->company = $companyObj;
}



/**
 * Middleware to validate existing login or redirect to show the login page.
 **/
function validateUserLogin( \Slim\Route $router ) {
	global $app;

	$company = $app->company;

	$redirect = false;
	$pattern = explode("/", $router->getPattern());
	if( count($pattern)>=3 ) {
		$redirect = $pattern[2];
	}
	
	if( isset($_SESSION['logged_in']) && $_SESSION['logged_in']===true ) {
		return true;
	} else {

		//temporal validation to allow access for non-existing companies
		if($company->id!==0) {
			$root_uri = $app->request->getRootUri();
			$url = $root_uri.'/'.$company->code.'/login';

			if($redirect) {
				$url .= '?redirect='.$redirect;
			}
			$app->redirect($url);
		}
	}
}


/** Function for the Authenthication Callback of the Pusher Library **/
function pusherAuth( $companyName ) {
	
	$socket_id = $_POST['socket_id'];
	$channel_name = $_POST['channel_name'];

	$app_key = '7a462160a94c5a4937f3';
	$secret_key = '81bb1067c28e7ee9a577';

	$hash = hash_hmac("sha256", $socket_id.':'.$channel_name, $secret_key);

	$response = array('auth'=>$app_key.':'.$hash);

	echo json_encode($response);
	die();
}