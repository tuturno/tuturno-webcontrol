<?php
/**
 * Helpers Functions for Requests
 * ------------------------------
 **/


/* Helper Function to make HTTP Requests and get JSON response */
function serviceQuery( $url, $contentType='json' ) {
	try {
		$ctx = stream_context_create( array('http' => array( 'timeout' => 5, ) ) );
		$data = file_get_contents($url, false, $ctx);
		//failed to open stream: Connection refused
		if($contentType==='json') {
			return json_decode($data);
		} else {
			return $data;
		}
	} catch(Exception $ex) {
		//return $ex->getMessage();
		return null;
	}
}