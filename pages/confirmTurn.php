	
	<section class="confirmpage">
    	<div class="queue-info" ng-controller="confirmTurnController as confirm">

            <h1>Turno asignado correctamente</h1>
            <h3 ng-show="cellNumber">La información será enviada al Celular: <span class="info-cell">{{cellNumber}}</span></h3>

            <hr />

    		<h2 class="currentTurn"><label>Turno Asignado:</label> <span class="turns">{{queue.lastAssignedTurn}}</span> </h2>
    		<h2 class="countdown"><label>Tiempo promedio de espera:</label>
    			<span>
    			<!-- <timer countdown="queue.averageTime" interval="1000"> {{hhours}} hora{{hoursS}}, {{mminutes}} minuto{{minutesS}}</timer> -->
                    <span class="time">{{remainingTime|durationview}}</span>
    			</span>
    		</h2>
            
    	</div>

    	<div class="round-button big-button"><div class="round-button-circle">
            <button ng-click="finishTurn()" id="new-turn-button" class="round-button" 
                tabindex="1">Nuevo Turno</button>
        </div></div>
        <script>
        $rootScope.$on('$includeContentRequested', function() {
            //jQuery('#new-turn-button').focus();
            //console.log( 'asd' );
        });
        </script>
  	</section>