	
	<section class="homepage">
    	<div class="queue-info" ng-controller="homeController as home">
    		<div class="currentTurn circle">
                <div class="label">Turnos en espera:</div>
                <div class="turns">{{home.queue.waitingTurns}}</div>
            </div>
    		<div class="countdown circle">
                <div class="label">Tiempo de espera:</div>
    			<div class="timer">
    			     <!--<timer countdown="home.queue.averageTime" interval="1000">{{hhours}}:{{mminutes}}</timer> -->
                     <span class="time">{{remainingTime|durationview}}</span>
    			</div>
    		</div>
            <div class="clear"></div>
    	</div>
    	<div class="round-button big-button"><div class="round-button-circle">
            <a href="#request" class="round-button">Generar Turno</a>
        </div></div>
  	</section>