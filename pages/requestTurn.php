<section class="requestturnpage">
	<div class="row" ng-controller="requestTurnController">
		<div class="col-xs-12 col-sm-6">
            <label class="label-phone">Número Celular</label>
            <div id="showNumber" class="shownumber">
                <input type="number" value="{{cellNumber}}" 
                        ng-model="cellNumber" ngMaxlength="10" ng-keyup="checkEnterKey($event)" />
            </div>
            
            <label class="label-phone">Turno</label>
            <div id="showTurnNumber" class="shownumber">
                <input type="number" value="{{turnNumber}}" 
                        ng-model="turnNumber" ngMaxlength="4" ng-keyup="checkEnterKey($event)" />
            </div>            

            <div class="keyboard">
            <?php
            $k=1;

            for($i=0; $i<3; $i++) {
                for($j=0; $j<3; $j++) {
                    echo '<button class="key-btn" ng-click="addNumber('.$k.')">'.$k.'</button>';
                    echo "\n";
                    $k++;
                }
            }
            
            echo '<button class="key-btn" ng-click="addNumber(0)">0</button> ';
            ?>
                <div class="round-button small-button erase"><div class="round-button-circle">
                    <button class="round-button" ng-click="deleteNumber()">Borrar</button>
                </div></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="currentTurn circle">
                <div class="label">Turnos en espera</div>
                <div class="turns">{{queue.waitingTurns}}</div>
            </div>

            <div class="countdown circle pull-left">
                <div class="label">Tiempo de espera:</div>
                <div class="timer">
                    <span class="time">{{remainingTime|durationview}}</span>
                </div>
                <div class="text-center info-time">hh:mm</div>
            </div>

            <div class="clear clearfix"></div>
            
            <div id="showMessage">
                <div class="input-group-addon"></div>
            </div>

            <div id="control-buttons">
                <div class="round-button big-button"><div class="round-button-circle">
                    <a ng-click="confirmTurn()" class="round-button">Generar Turno</a>
                </div></div>                
            </div>
        </div>
	</div>    	
</section>