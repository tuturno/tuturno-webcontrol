<?php
/**
 * Company Controller
 *
 * @author Julian Lamprea
 * @package webcontrol.tuturno
 * @version 1.0
 */
function companyController( $companyName ) {
	global $app;

	$root_uri = $app->request->getRootUri();

	$company = $app->company;
	//die("<pre>".print_r($company, true)."</pre>");

	if(isset($company->current_queue)) {
		$company->lastAssignedTurn = $company->current_queue->lastAssignedTurn;
		$company->currentTurn = $company->current_queue->currentTurn;

		//fixed but to integrate when there are no data
		if(empty($company->current_queue->lastTurnCallDatetime)) {
			$company->current_queue->lastTurnCallDatetime = 0;
		}

		if( $company->lastAssignedTurn >= $company->currentTurn ) {
			$company->waitingTurns = $company->lastAssignedTurn - $company->currentTurn;
		}
		else {
			$company->waitingTurns = $company->current_queue->maxTurn - $company->currentTurn + $company->lastAssignedTurn;
		}

	} else {
		$company->lastAssignedTurn = 40; //demo by default
		$company->currentTurn = 24;
		$company->waitingTurns = $company->lastAssignedTurn - $company->currentTurn;
		$company->current_queue = null;

		$company->allow_turn_input = 1; //not used yet
	}
    
	$app->view()->setData( array(
		'root_uri' => $root_uri,
		'company' => $company,
		'company_url' => $root_uri.'/'.$company->code
	));
    
    $app->render('company.php');

}


function nextTurn( $company ) {
	global $app;
	
	$root_uri = $app->request->getRootUri();

	$company = $app->company;

	if(isset($company->current_queue)) {
		$company->lastAssignedTurn = $company->current_queue->lastAssignedTurn;
		$company->currentTurn = $company->current_queue->currentTurn;

		if( $company->lastAssignedTurn===0 ) {
			$company->waitingTurns = 0;
		} else if( $company->lastAssignedTurn >= $company->currentTurn ) {
			$company->waitingTurns = $company->lastAssignedTurn - $company->currentTurn;
		}
		else {
			$company->waitingTurns = $company->current_queue->maxTurn - $company->currentTurn + $company->lastAssignedTurn;
		}

	} else {
		$company->lastAssignedTurn = 40;//demo by default
		$company->currentTurn = 1;//demo by default
		$company->waitingTurns = $company->lastAssignedTurn - $company->currentTurn;
		$company->current_queue = null;
	}
	
	$app->view()->setData( array(
		'root_uri' 		=> $root_uri,
		'company'		=> $company,
		'company_url' 	=> $root_uri.'/'.$company->code
	));

	$app->render('next.php');
}


function showTurns( $company ) {
	global $app;
	
	$root_uri = $app->request->getRootUri();

	$company = $app->company;
	
	if(isset($company->current_queue)) {
		$company->currentTurn = $company->current_queue->currentTurn;
	} else {
		$company->currentTurn = 1;//demo by default
		$company->current_queue = null;
	}
    
	$app->view()->setData( array(
		'root_uri' 		=> $root_uri,
		'company'		=> $company,
		'company_url' 	=> $root_uri.'/'.$company->code
	));

	$app->render('showCurrentTurn.php');
}




/**
 * Process the Ajax Request for Login a Valid User
 * The user should be previously validated against the TuTurno-Core Server
 **/
function processUserLogin() {
	// TODO: Add cookie with XSRF-TOKEN to add Cross Site Request Forgery (XSRF) Protection

	global $app;

	$user = json_decode( $app->request->get('data') );
	//die("<pre>".print_r($user, true)."</pre>");
	
	$_SESSION['logged_in'] = true;
	$_SESSION['user'] = $user;

	$redirect = $app->request->get('redirect');

	$root_uri = $app->request->getRootUri();
	$url = $root_uri.'/'.$user->company->code;
	if($redirect) {
		$url .= '/'.$redirect;
	}
	$app->redirect($url);
}

/** user logout **/
function processUserLogout() {
	global $app;

	$_SESSION['logged_in'] = false;
	$_SESSION['user'] = null;

	unset($_SESSION['logged_in']);
	unset($_SESSION['user']);

	$root_uri = $app->request->getRootUri();
	$app->redirect($root_uri.'/'.$app->company->code);
}


function showLoginPage( $company=null ) {
	global $app;

	$companyObj = $app->company;

	$root_uri = $app->request->getRootUri();

	$app->view()->setData( array(
		'root_uri' => $root_uri,
		'company' => $companyObj,
		'company_url' => $root_uri.'/'.$company
	));
    $app->render('login.php');
}