/** AngularJS initiation **/
var app = angular.module('TuTurnoApp', ["ngRoute"]);

/** Pusher Client **/
var pusher = null;
var channel = null;
var subscription_succeeded = false;

window.onbeforeunload = function (e) {
  if(pusher!==null) {
    console.log( 'disconnecting pusher' );
    pusher.disconnect();
  }
};


app.controller("showCurrentTurnController", function($scope) {
    $scope.queue = currentQueue;
    pusherConnect();

    if(channel!==null) {
        channel.bind('client-turn-updated', function(data) {
            $scope.$apply(function(){
                $scope.queue.currentTurn = data.data.currentTurn;
            });
        });
    }
});



app.controller("nextTurnController", function($scope, $http, $location, $window) {
    $scope.queue = currentQueue;

    validateAssignedTurns(currentQueue.lastAssignedTurn);

    pusherConnect();

    $scope.nextTurnChange = function() {
        var r = confirm("Confirmar Paso de Turno");
        if( r === true ) {

            var turnURL = serverURL+'turnCall/nextTurn.json';
            var data = {queue: currentQueue.id };

            $http({method: 'POST', url: turnURL, data:data, responseType:'json'}).
            success(function(data, status, headers, config) {
                //console.log( data );
                if(data.status===undefined) {
                    if(subscription_succeeded) {
                        var turnData = {currentTurn: data.turnCall.number, averageTime: data.queue.turnCallsTimeLapseAverage};
                        var triggered = channel.trigger('client-turn-updated', {data:turnData} );
                        if(triggered) {
                            //console.log( 'notification triggered' );
                            $scope.queue.currentTurn = data.turnCall.number;
                            $scope.queue.waitingTurns = calculateWaitingTurns(data.queue.lastAssignedTurn, data.turnCall.number);
                            validateAssignedTurns(data.queue.lastAssignedTurn);
                        }
                    } else {
                        $scope.queue.currentTurn = data.turnCall.number;
                    }
                } else {
                    alert('No hay turnos pendientes!');
                }
            }).error(function(data, status, headers, config) {
                alert( "ERROR: " + status );
            });
        }
    };
});

function validateAssignedTurns(lastAssignedTurn) {
    
    if( lastAssignedTurn===0 ) {
        document.getElementById("showMessage").className = "has-error";
        document.getElementById("showMessageContent").innerHTML = "Cola sin turnos asignados";
    } else {
        document.getElementById("showMessage").className = "";
        document.getElementById("showMessageContent").innerHTML = "";
    }


}

function calculateWaitingTurns(lastAssignedTurn, currentTurn) {
    if(lastAssignedTurn===0){
        return 0;
    } else if( lastAssignedTurn >= currentTurn ) {
        return lastAssignedTurn - currentTurn;
    } else {
        return currentQueue.maxTurn - currentTurn + lastAssignedTurn;
    }
}


function pusherConnect() {
    if( pusher===null ) {
        Pusher.log = function(message) {
          if (window.console && window.console.log) {
            window.console.log(message);
          }
        };

        pusher = new Pusher('7a462160a94c5a4937f3', {authEndpoint : company_url+'/pusher/auth'});
        channel = pusher.subscribe('private-channel-'+company_code);
        channel.bind('pusher:subscription_succeeded', function() {
            console.log( 'subscription_succeeded' );
            subscription_succeeded = true;
        });
    }
}