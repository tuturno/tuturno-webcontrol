var app = angular.module('TuTurnoApp', ["ngRoute","mobile-angular-ui"]);


// configure our routes
app.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.php',
            controller  : 'homeController'
        })

        // route for the about page
        .when('/request', {
            templateUrl : 'pages/requestTurn.php',
            controller  : 'requestTurnController'
        })

        // route for the contact page
        .when('/confirm', {
            templateUrl : 'pages/confirmTurn.php',
            controller  : 'confirmTurnController'
        })
        .otherwise({ redirectTo: '/' });
});


var body = document.getElementsByTagName('body')[0];

setTimeout(function() {
    body.setAttribute('ng-app', 'TuTurnoApp');
    angular.bootstrap(body, ['ng', 'TuTurnoApp']);
}, 1000);

/**** FastClick fix for mobile devices ****/
window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);

window.onbeforeunload = function (e) {
  if(pusher!==null) {
    console.log( 'disconnecting pusher' );
    pusher.disconnect();
  }
};


/**** Global Data Access ****/
var lastCellNumber = null;
var pusher = null;
var channel = null;
var subscription_succeeded = false;


/*================================
 * General Functions
 *==============================*/


function showErrorMessage( error_msg ) {
    jQuery('#showNumber').addClass('border-error');
    jQuery('#showMessage').addClass('has-error');
    jQuery('#showMessage .input-group-addon').html( error_msg );
}

function clearErrorMessages() {
    if( jQuery('#showMessage').hasClass('has-error') ) {
        jQuery('#showNumber').removeClass('border-error');
        jQuery('#showMessage').removeClass('has-error');
        jQuery('#showMessage .input-group-addon').html( '' );
    }
}

function initPusher($scope) {
    if( pusher===null ) {
        Pusher.log = function(message) {
          if (window.console && window.console.log) {
            window.console.log(message);
          }
        };

        pusher = new Pusher('7a462160a94c5a4937f3', {authEndpoint : company_url+'/pusher/auth'});
        channel = pusher.subscribe('private-channel-'+company_code);
    }

    channel.bind('pusher:subscription_succeeded', function() {
        console.log( 'subscription_succeeded' );
        subscription_succeeded = true;
    });
    channel.bind('client-turn-updated', function(data) {
        $scope.$apply(function() {
            var waiting = calculateWaitingTurns(currentQueue.lastAssignedTurn, data.data.currentTurn);
            currentQueue.averageTime = data.data.averageTime;

            $scope.calculateTimer();
            $scope.queue.waitingTurns = waiting;
        });
    });
}


function calculateWaitingTurns(lastAssignedTurn, currentTurn) {
    
    if( lastAssignedTurn >= currentTurn ) {
        return lastAssignedTurn - currentTurn;
    } else {
        return currentQueue.maxTurn - currentTurn + lastAssignedTurn;
    }
}