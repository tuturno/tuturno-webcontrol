
// manual timer
// http://www.byteblocks.com/Post/Create-countdown-clock-using-angularJS-services
app.factory('datetime', ['$timeout', function ($timeout) {
    var duration = function (timeSpan) {
        var days = Math.floor(timeSpan / 86400000);
        var diff = timeSpan - days * 86400000;
        var hours = Math.floor(diff / 3600000);
        diff = diff - hours * 3600000;
        var minutes = Math.floor(diff / 60000);
        diff = diff - minutes * 60000;
        var secs = Math.floor(diff / 1000);
        return { 'hours': hours, 'minutes': minutes, 'seconds': secs };
    };
    function getRemainigTime(referenceTime) {
        var now = moment().utc();
        return moment(referenceTime) - now;
    }
    return {
        duration: duration,
        getRemainigTime: getRemainigTime
    };
}]);

app.filter('durationview', ['datetime', function (datetime) {
    return function (input, css) {
        var duration = datetime.duration(input);
        //return duration.hours + ":" + duration.minutes + ":" + duration.seconds;
        if(parseInt(duration.hours,10)<10) { duration.hours = '0'+duration.hours; }
        if(parseInt(duration.minutes,10)<10) { duration.minutes = '0'+duration.minutes; }
        return duration.hours + ":" + duration.minutes;
    };
}]);


app.controller("homeController", ["$scope", "$timeout", "datetime", function($scope, $timeout, datetime) {
    this.queue = currentQueue;

    $scope.averageTurnTime = 0;
    $scope.remainingTime = 0;

    var processTimer = function () {
        if(currentQueue.waitingTurns>0)
            $scope.remainingTime = datetime.getRemainigTime( $scope.averageTurnTime );
    };

    var tick = function () {
        processTimer();
        $timeout(tick, 1000);
    };

    var calculateTimer = function() {
        var r =  currentQueue.averageTime;
        if(currentQueue.waitingTurns>0) {
            r *= currentQueue.waitingTurns;
        }

        // restar aqui lo del averageTime - diferecnia con el ultimo turno pedido.
        if( currentQueue.lastTurnCallDatetime>0 ) {
            var now = new Date().getTime();
            var diff = now - currentQueue.lastTurnCallDatetime;
            console.log( diff/1000 );

            r = r-diff;

            currentQueue.lastTurnCallDatetime = 0;
        }
        
        $scope.averageTurnTime = moment().add('seconds', r );

        processTimer();
    };
    $scope.calculateTimer = calculateTimer;

    calculateTimer();
}]);



app.controller("confirmTurnController", function($scope, $location, $timeout, datetime) {
    $scope.queue = currentQueue;
    $scope.cellNumber = lastCellNumber;

    $scope.averageTurnTime = 0;
    $scope.remainingTime = 0;

    var processTimer = function () {
        if(currentQueue.waitingTurns>0)
            $scope.remainingTime = datetime.getRemainigTime( $scope.averageTurnTime );
    };

    var tick = function () {
        processTimer();
        $timeout(tick, 1000);
    };

    var calculateTimer = function() {
        console.log( 'Recalculating Timer on seconds: ' + currentQueue.averageTime );
        var r =  currentQueue.averageTime;
        if(currentQueue.waitingTurns>0) {
            r *= currentQueue.waitingTurns;
        }
        $scope.averageTurnTime = moment().add('seconds', r );
        processTimer();
    };
    $scope.calculateTimer = calculateTimer;

    calculateTimer();

    $scope.finishTurn = function() {
        $scope.cellNumber = null;
        lastCellNumber = null;
        $location.path( "/request" );
    };
});



app.controller("requestTurnController", function($scope, $http, $location, $timeout, datetime) {
    $scope.cellNumber = lastCellNumber;
    $scope.queue = currentQueue;

    $scope.averageTurnTime = 0;
    $scope.remainingTime = 0;

    var processTimer = function () {
        if(currentQueue.waitingTurns>0) {
            $scope.remainingTime = datetime.getRemainigTime( $scope.averageTurnTime );
        } else {
            $scope.remainingTime = 0;
        }
    };

    var tick = function () {
        processTimer();
        $timeout(tick, 1000);
    };

    var calculateTimer = function() {
        console.log( 'Recalculating Timer on seconds: ' + currentQueue.averageTime );
        var r =  currentQueue.averageTime;
        if(currentQueue.waitingTurns>0) {
            r *= currentQueue.waitingTurns;
        }
        
        //TODO: restar aqui lo del averageTime - diferencia con el ultimo turno pedido.        
        if( currentQueue.lastTurnCallDatetime>0 && r>0 ) {
            var now = new Date().getTime();
            var diff = now - currentQueue.lastTurnCallDatetime;
            console.log( diff/1000 );

            r = r-diff;

            currentQueue.lastTurnCallDatetime = 0;
        }

        $scope.averageTurnTime = moment().add('seconds', r );
        processTimer();
    };
    $scope.calculateTimer = calculateTimer;
    
    calculateTimer();
    $timeout(tick, 1000);
    initPusher( $scope );

    $scope.addNumber = function(keyNumber) {
        clearErrorMessages();
        if($scope.cellNumber!==null) {
            if($scope.cellNumber.toString().length<10) {
                //console.log( $scope.cellNumber );
                $scope.cellNumber = Number($scope.cellNumber.toString() + keyNumber);
            }
        } else {
            $scope.cellNumber = Number(keyNumber);
        }
    };

    $scope.deleteNumber = function() {
        if($scope.cellNumber!==null) {
            var numberString = $scope.cellNumber.toString();
            if(numberString.length > 0) {
                $scope.cellNumber = Number( numberString.substring(0, numberString.length-1) );
            }

            if($scope.cellNumber===0){
                $scope.cellNumber = null;
            }
        }
    };

    $scope.checkEnterKey = function( keyEvent ) {
        if( keyEvent.keyCode == 13 ) { // 13 => enter key
            validateAndConfirmTurn();
        }
    };

    $scope.confirmTurn = validateAndConfirmTurn;

    function validateAndConfirmTurn() {
        clearErrorMessages();

        if($scope.cellNumber!==null) {
            if($scope.cellNumber.toString().length==10) {

                lastCellNumber = $scope.cellNumber;

                increaseTurn(lastCellNumber, currentQueue, $location, $http, $scope.turnNumber);

            } else {
                showErrorMessage("Por favor completa el número telefónico");
            }
        } else {

            var r = confirm("¿Estas seguro de generar un Turno Anónimo?");
            if( r===true ) {
                //turn generated without a subscriber
                lastCellNumber = null;

                increaseTurn(lastCellNumber, currentQueue, $location, $http, $scope.turnNumber);

            } else {
                showErrorMessage("Por favor ingresa el número telefónico");
            }
        }
    }

    /** Function that assign a new turn to the queue passed by parameter and update the queue **/
    var increaseTurn = function(phoneNumber, queue, $location, $http, turnNumber) {

        var turnURL = serverURL+'numericTurn/newTurn.json';
        var queueURL = serverURL+'numericQueue/show/';

        var data = { queue: queue.id };
        if( phoneNumber!==null ){
            data.phoneNumber = phoneNumber.toString();
        }

        if( turnNumber!==null ) {
            data.number = turnNumber;
        }
        
        $http({method: 'POST', url: turnURL, data:data, responseType:'json'}).
        success(function(data, status, headers, config) {
            
            queueURL += queue.id +'.json';
            $http({method: 'POST', url: queueURL, responseType:'json'}).
            success(function(data2, status2, headers2, config2) {
                //console.log( data2 );
                currentQueue.lastAssignedTurn  = data2.lastAssignedTurn;

                currentQueue.waitingTurns = calculateWaitingTurns(data2.lastAssignedTurn, data2.currentTurn);

                //currentQueue.averageTime -= 60; // reduced 60 seconds to simulate a turn change
                if(data2.turnCallsTimeLapseAverage===0) {
                    currentQueue.averageTime = 20 * 60; //20min as default
                } else {
                    currentQueue.averageTime = data2.turnCallsTimeLapseAverage;
                }

                calculateTimer();

                var turnData = {currentTurn: data2.currentTurn, averageTime: currentQueue.averageTime};
                var triggered = channel.trigger('client-turn-updated', {data:turnData} );
                if(triggered) {
                    console.log( 'notification triggered' );
                }

                $location.path( "/confirm" ); // TODO: Fix the redirect, it is not working
            }).error(function(data2, status2, headers2, config2) {
                if( data2.errors )
                    showErrorMessage( data2.errors[0].message );

                console.log( 'Error:' );
                console.log( data2 );
            });
        }).
        error(function(data, status, headers, config) {
            if( data.errors )
                showErrorMessage( data.errors[0].message );

            console.log( 'Error:' );
            console.log( data );
        });
    };
    
});