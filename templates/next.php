<!-- Interfaz para paso de turnos -->

<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Cambio de Turno - <?php echo ($company!=null ? $company->code : 'demo'); ?> | TuTurno</title>   

<meta name="description" content="Plataforma Web para el Registro o Solicitud de Nuevos Turnos" /> 

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" /> 
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>

<!-- CSS Styles -->
<link href="<?php echo $root_uri; ?>/css/base.css" rel="stylesheet"/>
<link href="<?php echo $root_uri; ?>/css/next.css" rel="stylesheet"/>

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

<!-- Fav Icon -->
<link rel="shortcut icon" href="<?php echo $root_uri; ?>/images/icons/16px.png">

<link rel="apple-touch-icon" href="<?php echo $root_uri; ?>/images/icons/36px.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $root_uri; ?>/images/icons/114px.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $root_uri; ?>/images/icons/72px.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $root_uri; ?>/images/icons/144px.png">

</head>

<body class="next-turn <?php echo $company->code; ?>" ng-app="TuTurnoApp">
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-route.min.js"></script>

	<div class="fullwidth">
		<div class="row topnav">
			<a href="<?php echo $company_url; ?>/next">
				<img src="http://tuturnoapp.com/img/TuTurnoLogo.png" alt="Logo TuTurno" height="37" />
			</a>
		</div>

		<div class="content wrapper" ng-controller="nextTurnController">
			<div class="currentTurn circle">
	            <div class="label">Turno Actual</div>
	            <div class="turns">{{queue.currentTurn}}</div>
	        </div>

	        <div class="round-button big-button"><div class="round-button-circle">
                <a ng-click="nextTurnChange()" class="round-button">Pasar Turno</a>
            </div></div>

            <div class="waitingTurns circle">
                <div class="label">Turnos en espera</div>
                <div class="turns">{{queue.waitingTurns}}</div>
            </div>

		</div>

		<div id="showMessage">
            <div id="showMessageContent" class="input-group-addon"></div>
        </div>

		<div class="footer">
	    	<img src="<?php echo $root_uri; ?>/images/<?php echo $company->code; ?>-logo-width.png" 
	    		 alt="Logo" class="logo" height="60">
	    </div>
	</div>

<script src="<?php echo $root_uri; ?>/js/mobile-angular-ui.min.js"></script>
<script src="<?php echo $root_uri; ?>/js/fastclick.js"></script>
<script src="<?php echo $root_uri; ?>/js/next-turn-app.js"></script>
<script src="//js.pusher.com/2.2/pusher.min.js"></script>
<script>
<?php
echo "var company_code = '". ($company!=null ? $company->code : 'demo')."';\n";
echo "var company_url = '". $company_url ."';\n";
echo "var serverURL = '".TUTURNO_SERVER."';\n";
?>

	/**** Global Data from the API Server ****/
	var currentQueue = {
		id : <?php echo (($company->current_queue) ? $company->current_queue->id : 0); ?>,
		currentTurn : <?php echo $company->currentTurn; ?>,
		lastAssignedTurn : <?php echo $company->lastAssignedTurn; ?>,
		waitingTurns : <?php echo $company->waitingTurns; ?>,
		maxTurn : <?php echo (($company->current_queue) ? $company->current_queue->maxTurn : 99); //99 as default ?>
	}
</script>
</body>
</html>