<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Selector de turno - <?php echo ($company!=null ? $company->code : 'demo'); ?> | TuTurno</title>   

<meta name="description" content="Plataforma Web para el Registro o Solicitud de Nuevos Turnos" /> 

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" /> 
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="960"/>

<!-- CSS Styles -->
<link href="css/mobile-angular-ui-base.min.css" rel="stylesheet">
<link href="css/base.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

<!-- Fav Icon -->
<link rel="shortcut icon" href="<?php echo $root_uri; ?>/images/icons/16px.png">

<link rel="apple-touch-icon" href="<?php echo $root_uri; ?>/images/icons/36px.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $root_uri; ?>/images/icons/114px.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $root_uri; ?>/images/icons/72px.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $root_uri; ?>/images/icons/144px.png">

<!-- Analytics -->
<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'Insert Your Code']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

</script>
<!-- End Analytics -->

</head>


<body class="<?php echo $company->code; ?>">
	<div class="splash" ng-cloak="">
    	<h2><img alt="TuTurno" src="http://tuturnoapp.com/img/TuTurnoLogo.png"><br />Iniciando Aplicación</h2>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-route.min.js"></script>

	
	<!-- Homepage TuTurno -->
	<div class="fullwidth">
		<div class="row topnav">
			<a href="<?php echo $company_url; ?>" tabindex="2">
				<img src="http://tuturnoapp.com/img/TuTurnoLogo.png" alt="Logo TuTurno" height="37" />
			</a>
		</div>

	<div class="container-fluid wrapper" ng-cloak="">
		<!-- logo -->
		<!-- <div class="row">
			<div class="col-xs-6">
				<img src="images/santafe-logo.png" height="180">
			</div>
			<div class="col-xs-6" style="text-align:right;">
				<img src="images/santafe-promo-banner.png" height="180">
			</div>
		</div> -->

		<div id="main">
			<!-- injected views -->
			<div ng-view></div>
		</div>

		<?php
		//$messages = $this->getData('flash')->getMessages();
		if( isset($flash['server_errors']) ) {
			echo "<div class='has-error'><pre class='input-group-addon'>".print_r($flash['server_errors'], true)."</pre></div>";
		}
		?>

	</div>
	</div>
	<!-- End Homepage -->
<script>
	<?php
	echo "var company_code = '". ($company!=null ? $company->code : 'demo')."';\n";
	echo "var company_url = '". $company_url ."';\n";

	if( $_SERVER['HTTP_HOST']==='localhost' ) {
		echo "var serverURL = 'http://localhost:8080/services/';\n";
	} else {
		echo "var serverURL = 'http://tuturno.elasticbeanstalk.com/';\n";
	}


	$average = ($company->current_queue) ? $company->current_queue->turnCallsTimeLapseAverage : 0;
	if($average===0) {
		$average = 20 * 60; //20min by default - TODO: change to a custom rule for company
	}
	?>

	/**** Global Data from the API Server ****/
	var currentQueue = {
		id 				 : <?php echo (($company->current_queue) ? $company->current_queue->id : 0); ?>,
		lastAssignedTurn : <?php echo $company->lastAssignedTurn; ?>,
		waitingTurns 	 : <?php echo $company->waitingTurns; ?>,
		maxTurn 		 : <?php echo (($company->current_queue) ? $company->current_queue->maxTurn : 99); //99 as default ?>,
		averageTime 	 : <?php echo $average; ?>,
		lastTurnCallDatetime : <?php echo (($company->current_queue) ? $company->current_queue->lastTurnCallDatetime : 0) ?>
	}
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//js.pusher.com/2.2/pusher.min.js"></script>
<script src="js/mobile-angular-ui.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/fastclick.js"></script>
<script src="js/app.js"></script>
<script src="js/controllers.js"></script>

</body>
</html>