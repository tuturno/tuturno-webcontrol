<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>TuTurno | Selector de turno</title>   

<meta name="description" content="Insert Your Site Description" /> 

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" /> 
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="960"/>

<!-- CSS Styles -->
<link href="<?php echo $root_uri; ?>/css/mobile-angular-ui-base.min.css" rel="stylesheet">
<link href="<?php echo $root_uri; ?>/css/base.css" rel="stylesheet">
<link href="<?php echo $root_uri; ?>/css/login.css" rel="stylesheet">

<!-- Google Font -->
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900' rel='stylesheet' type='text/css'>

<!-- Fav Icon -->
<link rel="shortcut icon" href="<?php echo $root_uri; ?>/images/icons/16px.png">

<link rel="apple-touch-icon" href="<?php echo $root_uri; ?>/images/icons/36px.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $root_uri; ?>/images/icons/114px.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $root_uri; ?>/images/icons/72px.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $root_uri; ?>/images/icons/144px.png">

<!-- Analytics -->
<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'Insert Your Code']);
  _gaq.push(['_trackPageview']);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

</script>
<!-- End Analytics -->

</head>


<body class="login" ng-app="TuTurnoApp">
	<div class="splash" ng-cloak="" style="opacity:1">
		
	  <div id="main-col" class="container-fluid wrapperLogin" >
		<div class="login-container" ng-controller="loginController as loginCtrl">
		  <div class="login-content">
			<img alt="Logo TuTurno" src="http://www.tuturnoapp.com/img/logoAppTuTurno.png" id="logo">

			<?php if ($company->current_queue) : ?>

			<h4 class="title border-bottom">Inicia Sesión</h4>
			  
			
			<form ng-submit="signinUser()" method="post" name="login-form" id="login-form">
				<input  type="hidden" name="redirect" ng-model="redirect" id="redirect"
						value="<?php echo isset($_GET['redirect']) ? $_GET['redirect'] : ''; ?>" />
			   <div class="login-input">
				<div class="control-group">
				 <div class="controls ">
				  
				  <input type="hidden" name="targetUri" value="/">
				  <input type="text" name="username" id="username" 
						 placeholder="usuario" ng-model="username">
				 </div>
				</div>
				<div class="control-group">
				 <div class="controls">
				  <input type="password" name="password" id="password" 
						 placeholder="contraseña" ng-model="password">
				 </div>
				</div>
			   </div>
			   <div class="login-actions">
				<span class="pull-left" ng-show="error_msg">{{error_msg}}</span>
				<span class="pull-right clearfix">
				 <button type="submit" class="btn btn-primary">
				  Iniciar
				 </button>
				</span>
			   </div>               
			</form>
			
			<?php else : ?>

			<h4 class="title border-bottom text-center">Inicio de Sesión no autorizado!</h4>

			<?php endif; ?>

		   </div>
		</div>    
	  </div>


	</div>
	
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular-route.min.js"></script>
<script src="<?php echo $root_uri; ?>/js/mobile-angular-ui.min.js"></script>
<script>
<?php
echo "var company_id = ". ($company!=null ? $company->id : '0').";\n";
echo "var serverURL = '".TUTURNO_SERVER."';";
?>

var loginURL = serverURL+'remoteAuth/signin.json';

var app = angular.module('TuTurnoApp', ["ngRoute", "mobile-angular-ui"]);
app.controller("loginController", function($scope, $http, $location) {
	
	$scope.username = null;
	$scope.password = null;
	$scope.error_msg = null;
	$scope.redirect = '<?php echo isset($_GET['redirect']) ? $_GET['redirect'] : ''; ?>';

	$scope.signinUser = function() {
		//console.log( 'trying to signin the user' );

		ajaxURL = loginURL+'?username='+$scope.username+'&password='+$scope.password;

		if($scope.username!==null && $scope.password!==null) {
			$http({method: 'GET', url: ajaxURL}).
			success(function(data, status, headers, config) {
				// when the response is available
				console.log( 'OK' );
				//user found and signed-in ok
				if( data.status==200 ) {
					if( data.user.company.id===company_id ) {
				  		$scope.error_msg = null
						
				  		var processLoginURL = '<?php echo $company_url; ?>/processUserLogin';
						var params = encodeURIComponent( JSON.stringify(data.user) );

						window.location = processLoginURL+'?redirect='+$scope.redirect+'&data='+params;

					} else {
				  		$scope.error_msg = "Acceso a empresa no correspondiente"
					}
				} else if( data.status==500 ) { // some error				
					// mostrar el error en un div para errores
					$scope.error_msg = data.message;
				} else { // unknow error

				}
		  	}).
			error(function(data, status, headers, config) {
				// if an error occurs or server returns response with an error status.
				console.log( 'server error' );
				console.log( data );
			});
		} else {
		  $scope.error_msg = "Debes ingresar tus credenciales";
		}
	}
});

</script>
</body>
</html>