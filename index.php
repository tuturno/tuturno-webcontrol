<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require 'vendor/autoload.php';

$app = new \Slim\Slim();
$app->config( array(
	'debug'=> true,
	'cookies.encrypt' => true,
	'templates.path' => './templates' 
));
$app->setName('tuturnoapp-control');

$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '30 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'czBVbjdVY2k4OA==',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));


if( $_SERVER['HTTP_HOST']==='localhost' ) {
	define('TUTURNO_SERVER','http://localhost:8080/services/');
} else {
	define('TUTURNO_SERVER','http://tuturno.elasticbeanstalk.com/');
    //define('TUTURNO_SERVER','http://192.168.0.12:8080/services/');
}


// Helpers utilities
require_once('helpers/request.php');
require_once('helpers/access-filters.php');

// define required controllers
require_once('controllers/company.php');

// define company routers
$app->get('/:company', 'validateCompanyAccess', 'validateUserLogin', 'companyController' );
$app->get('/:company/login', 'validateCompanyAccess', 'showLoginPage' );
$app->get('/:company/logout', 'validateCompanyAccess', 'processUserLogout' );
$app->get('/:company/next', 'validateCompanyAccess', 'validateUserLogin', 'nextTurn' );
$app->get('/:company/processUserLogin', 'processUserLogin' );
$app->get('/:company/show', 'validateCompanyAccess', 'showTurns' );
$app->post('/:company/pusher/auth', 'pusherAuth' );

$app->get('/', function () use ($app) {
	$app->view()->setData(array('foo' => 'bar'));
    $app->render('default.php');
});


$app->run();